module TaxiMeter(
clock, vip_enable, moving, reset_income, seat_0, seat_1, seat_2, seat_3,
s0cost, s1cost, s2cost, s3cost, vip_cost, overall_income);

input clock, vip_enable, moving, reset_income, seat_0, seat_1, seat_2, seat_3;
output reg[15:0] s0cost, s1cost, s2cost, s3cost, vip_cost, overall_income;

// bits from left to right indicate -> vip_enable, seat_3, seat_2, seat_1, seat_0, moving
reg [0:5] state;

initial
begin
	state = 6'b000000;
	s0cost = 16'D0;
	s1cost = 16'D0;
	s2cost = 16'D0;
	s3cost = 16'D0;
	vip_cost = 16'D0;
	overall_income = 16'D0;
end

always@(posedge clock)
begin
	if(seat_0 == 1'b0 && state[1] == 1'b1)
	begin
		state[1] = 1'b0;
		s0cost = 16'D0;
	end

	if(seat_0 == 1'b1)
	begin
		if(state[1] == 1'b0 && state[5] == 1'b0)
		begin
			state[1] = 1'b1;
			s0cost = s0cost + 16'D10;
			overall_income = overall_income + 16'D10;
		end
		else if(state[1] == 1'b1 && state[5] == 1'b0)
		begin
			if(state[0] == 1'b0)
			begin
				s0cost = s0cost + 16'D5;
				overall_income = overall_income + 16'D5;
			end
			else if(state[0] == 1'b1)
			begin
				s0cost = s0cost + 16'D8;
				overall_income = overall_income + 16'D8;			
			end
		end
	end

	if(seat_1 == 1'b0 && state[2] == 1'b1)
	begin
		state[2] = 1'b0;
		s1cost = 16'D0;	
	end

	if(seat_1 == 1'b1)
	begin
		if(state[2] == 1'b0 && state[5] == 1'b0)
		begin
			state[2] = 1'b1;
			s1cost = s1cost + 16'D10;
			overall_income = overall_income + 16'D10;
		end
		else if(state[2] == 1'b1 && state[5] == 1'b0)
		begin
			if(state[0] == 1'b0)
			begin
				s1cost = s1cost + 16'D5;
				overall_income = overall_income + 16'D5;
			end
			else if(state[0] == 1'b1)
			begin
				s1cost = s1cost + 16'D8;
				overall_income = overall_income + 16'D8;			
			end
		end
	end

	if(seat_2 == 1'b0 && state[3] == 1'b1)
	begin
		state[3] = 1'b0;
		s2cost = 16'D0;
	end

	if(seat_2 == 1'b1)
	begin
		if(state[3] == 1'b0 && state[5] == 1'b0)
		begin
			state[3] = 1'b1;
			s2cost = s2cost + 16'D10;
			overall_income = overall_income + 16'D10;
		end
		else if(state[3] == 1'b1 && state[5] == 1'b0)
		begin
			if(state[0] == 1'b0)
			begin
				s2cost = s2cost + 16'D5;
				overall_income = overall_income + 16'D5;
			end
			else if(state[0] == 1'b1)
			begin
				s2cost = s2cost + 16'D8;
				overall_income = overall_income + 16'D8;			
			end
		end
	end

	if(seat_3 == 1'b0 && state[4] == 1'b1)
	begin
		state[4] = 1'b0;
		s3cost = 16'D0;
	end

	if(seat_3 == 1'b1)
	begin
		if(state[4] == 1'b0 && state[5] == 1'b0)
		begin
			state[4] = 1'b1;
			s3cost = s3cost + 16'D10;
			overall_income = overall_income + 16'D10;
		end
		else if(state[4] == 1'b1 && state[5] == 1'b0)
		begin
			if(state[0] == 1'b0)
			begin
				s3cost = s3cost + 16'D5;
				overall_income = overall_income + 16'D5;
			end
			else if(state[0] == 1'b1)
			begin
				s3cost = s3cost + 16'D8;
				overall_income = overall_income + 16'D8;			
			end
		end
	end

	if(vip_enable == 1'b0 && state[5] == 1'b1)
	begin
		state[5] = 1'b0;
		vip_cost = 16'D0;
	end

	if(vip_enable == 1'b1)
	begin
		if(state[5] == 1'b0)
		begin
			state[5] = 1'b1;
			state[1] = 1'b0;
			state[2] = 1'b0;
			state[3] = 1'b0;
			state[4] = 1'b0;
			vip_cost = vip_cost + 16'D10;
			s0cost = 16'D0;
			s1cost = 16'D0;
			s2cost = 16'D0;
			s3cost = 16'D0;
			overall_income = overall_income + 16'D10;
		end
		else if(state[5] == 1'b1)
		begin
			if(state[0] == 1'b0)
			begin
				vip_cost = vip_cost + 16'D5;
				overall_income = overall_income + 16'D5;
			end
			if(state[0] == 1'b1)
			begin
				vip_cost = vip_cost + 16'D16;
				overall_income = overall_income + 16'D16;			
			end
		end
	end

	if(moving == 1'b0 && state[0] == 1'b1) state[0] = 1'b0;
	if(moving == 1'b1 && state[0] == 1'b0) state[0] = 1'b1;
end

always@(posedge reset_income)
begin
	overall_income = 16'D0;
end

endmodule